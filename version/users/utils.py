import os
import secrets
from PIL import Image
from flask import url_for, current_app
from requests import post as send_post

def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile_pics', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


def send_reset_email(user):
    MAILGUN_DOMAIN = 'sandboxe4d9734075ab496a999328f2ea91a9ea.mailgun.org'
    MAILGUN_API_KEY = 'd9ec44032d4585f7a83d6b84f90cfb77-fd0269a6-a62d63dd'
    FROM_TITLE = 'Reset Password'
    FROM_EMAIL = 'postmaster@sandboxe4d9734075ab496a999328f2ea91a9ea.mailgun.org'
    token = user.get_reset_token()
    response = send_post(
        f"https://api.mailgun.net/v3/{MAILGUN_DOMAIN}/messages",
        auth=("api", MAILGUN_API_KEY),
        data={
            'from': f"{FROM_TITLE} <{FROM_EMAIL}>",
            'to': user.email,
            'subject': 'RESET YOUR PASSWORD',
            'text': f'''To reset your password, visit the following link:
{url_for('users.reset_token', token=token, _external=True)}

If you did not make this request then simply ignore this email and no changes will be made.
''',
            'html':''
        }
    )
