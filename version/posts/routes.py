from flask import Blueprint, render_template, url_for, flash, abort, request, redirect
from flask_login import current_user, login_required
import numpy as np
from sklearn.linear_model import LinearRegression
import pickle

from version import db
from version.models import Post 
from version.posts.forms import PostForm
from version.posts.utils import save_dpicture

posts = Blueprint('posts', __name__)

@posts.route("/post/approved", methods=['GET','POST'])
def approved_post():
    page = request.args.get('page',1, type=int)
    posts= Post.query.filter_by(status='APPROVED').order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('posts.html', title='Approved Posts', posts=posts, cur_route='posts.approved_post')

@posts.route("/post/<int:post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)
    image_file = url_for('static', filename='post_image/' + post.image_file)
    return render_template('post.html', title=post.title, post=post, image_file=image_file)


@posts.route("/post/new", methods=['GET', 'POST'])
@login_required
def new_post():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user, disaster_type=form.disaster_type.data, longitude=form.longitude.data, latitude=form.latitude.data, area=form.area.data, floor=form.floor.data, landscape=form.landscape.data)
        print(form.picture.data)
        if form.picture.data:
            picture_file = save_dpicture(form.picture.data)
            post.image_file = picture_file
        filename = 'finalized_model.sav'
        loaded_model = pickle.load(open(filename, 'rb'))
        post.cost=loaded_model.predict(np.array([[post.area,post.floor]]))
        db.session.add(post)
        db.session.commit()
        flash('Your post has been created!', 'success')
        return redirect(url_for('main.home'))
    return render_template('create_post.html', title='New Post',
                           form=form, legend='New Post')

@posts.route('/post/awaited', methods=['GET','POST'])
@login_required
def awaited_post():
    if current_user.user_type != 'INTERMEDIATOR':
        abort(403)
    page = request.args.get('page',1, type=int)
    posts= Post.query.filter_by(status='AWAITED').order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('posts.html', title='Awaited posts', posts=posts, cur_route='posts.awaited_post')

@posts.route('/post/rejected', methods=['GET','POST'])
@login_required
def rejected_post():
    if current_user.user_type != 'INTERMEDIATOR':
        abort(403)
    page = request.args.get('page',1, type=int)
    posts= Post.query.filter_by(status='REJECTED').order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('posts.html', title='Rejected posts', posts=posts, cur_route='posts.rejected_post')

@posts.route('/post/all', methods=['GET','POST'])
@login_required
def all_post():
    if current_user.user_type != 'INTERMEDIATOR':
        abort(403)
    page = request.args.get('page',1, type=int)
    posts= Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('posts.html', title='All posts', posts=posts, cur_route='posts.all_post')

@posts.route('/post/reject/<int:post_id>', methods=['GET','POST'])
@login_required
def reject_post(post_id):
    if current_user.user_type != 'INTERMEDIATOR':
        abort(403)
    post = Post.query.get_or_404(post_id)
    post.status='REJECTED'
    db.session.commit()
    return redirect(url_for('posts.post', post_id=post.id))

@posts.route('/post/approve/<int:post_id>', methods=['GET','POST'])
@login_required
def approve_post(post_id):
    if current_user.user_type != 'INTERMEDIATOR':
        abort(403)
    post = Post.query.get_or_404(post_id)
    post.status='APPROVED'
    db.session.commit()
    return redirect(url_for('posts.post', post_id=post.id))
