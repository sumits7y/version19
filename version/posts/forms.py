from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectField, FloatField, IntegerField
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import DataRequired

class PostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    content = TextAreaField('Content', validators=[DataRequired()])
    disaster_type = SelectField('Disaster Type', choices=[('flood','Flood'),('cyclone','Cyclone'),('earthquake','Earthquake')])
    picture = FileField('Picture', validators=[FileAllowed(['jpg', 'png'])])
    longitude = FloatField('Longitude', validators=[DataRequired()])
    latitude = FloatField('Latitude', validators=[DataRequired()])
    area = IntegerField('Area (in cm<sup>2</sup>)', validators=[DataRequired()])
    floor = IntegerField('Floor', validators=[DataRequired()])
    landscape = SelectField('Landscape', choices=[('plateu','Plateu'),('mountains','Mountains')])
    submit = SubmitField('Post')