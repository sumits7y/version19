from flask import Blueprint, request, render_template

from version.models import Post

main = Blueprint('main', __name__)

@main.route("/")
@main.route("/home")
def home():
    page = request.args.get('page',1, type=int)
    posts= Post.query.filter_by(status='APPROVED').order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('home1.html', posts=posts,title='Home',cur_route='main.home')

@main.route("/about")
def about():
    return render_template('about.html', title='About')

@main.route('/donate')
def donate():
    return render_template('donate.html',title='Donate')
